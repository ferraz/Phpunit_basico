<?php
/**
 * Created by PhpStorm.
 * User: tobias
 * Date: 03/05/18
 * Time: 23:17
 */

use PHPUnit\Framework\TestCase;

use SON\Model\Product;

class ProductTest extends TestCase {

    public function testIfIdIsNull(){
        global $db;
        echo get_class($db);
        // vendor/bin/phpunit tests/ProductTest.php --bootstrap=tests/bootstrap.php
        $product = new SON\Model\Product();
        $this->assertNull($product->getId());
    }

    /*

    public function testSetAndGetName(){

        $product = new \SON\Model\Product();
        //Testando quando o valor não é atribuido para a propriedade da classe
        $this->assertNull($product->getNome());
        $product->setNome("Meu produto 1");
        //Testando o retorno da classe Produto
        $this->assertInstanceOf(\SON\Model\Product::class, $product);
        $this->assertEquals("Meu produto 1", $product->getNome());
    }



    public function testSetAndGetPrice(){

        $product = new SON\Model\Product();
        $this->assertNull($product->getPrice());
        $product->setPrice(10.10);
        $this->assertInstanceOf(\SON\Model\Product::class, $product);
        $this->assertEquals(10.10, $product->getPrice());

    }


    public function testSetAndGetQuantity(){

        $product = new \SON\Model\Product();
        $this->assertNull($product->getQuantity());
        $product->setQuantity(5);
        $this->assertInstanceOf(\SON\Model\Product::class, $product);
        $this->assertEquals(5, $product->getQuantity());

    }

    */

    public function testIfTotalIsNull(){


        $product = new \SON\Model\Product();
        $this->assertNull($product->getTotal());
    }

    /**
     * @dataProvider collectionData
     */
    public function testEncapsulate($property, $expected, $actual)
    {

        $product = new Product();

        $null = $product->{'get'.ucfirst($property)}();
        $this->assertNull($null);

        //chamando o método setNome e passando o valor atual.
        $product->{'set'.ucfirst($property)}($actual);
        $this->assertInstanceOf(\SON\Model\Product::class, $product);

        //Chamando o método getNome mais a propriedade.
        $actual = $product->{'get'.ucfirst($property)}();
        $this->assertEquals($expected, $actual);
    }

    // Provedor de dados - Prover uma gama de dados para um método de teste para testar muitas informações.
    // Exemplo: Você pode automatizar os testes de encapsulamento de uma classe
    public function collectionData()
    {
        return [

            ['nome', 'Produto 1', 'Produto 1'],
            ['price', 10.11, 10.11],
            ['quantity', 5, 5]

        ];
    }
}