<?php

/*
phpunit - Torna essas variáveis globais ao escopo dos testes.
*/

$db = new PDO('sqlite::memory');
/*
 * fh - Variável de leitura de disco
 * */
$fh = fopen(__DIR__ . '/schema.sql', 'r');
while($line = fread($fh, 4096)) {
    $db->exec($line);
}