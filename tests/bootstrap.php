<?php

/*
 * Para acessar a aplicação e as classes.
 * */
require __DIR__ . '/../vendor/autoload.php';

/*
 * chamar o arquivo para iniciar o banco de dados.
 */
require __DIR__ . '/data/load_schema.php';