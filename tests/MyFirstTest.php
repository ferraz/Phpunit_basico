<?php
/**
 * Created by PhpStorm.
 * User: tobias
 * Date: 03/05/18
 * Time: 22:26
 */

use SON\Area;

use PHPUnit\Framework\TestCase;

class MyFirstTest extends TestCase
{
    public function testArray()
    {
        $array = [2];
        $this->assertNotEmpty($array);
    }

    public function testArea()
    {
        $area = new Area();
        $this->assertEquals(6, $area->getArea(2,3));
    }
}
