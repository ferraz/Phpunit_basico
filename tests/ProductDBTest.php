<?php
/**
 * Created by PhpStorm.
 * User: tobias
 * Date: 06/05/18
 * Time: 19:03
 */

use PHPUnit\Framework\TestCase;

class ProductDBTest extends TestCase
{

    public function testIfProductIsSave(){
        global $db;
        $product = new SON\Model\Product($db);
        $result = $product->save([
            'name' => 'Produto 1',
            'price' => 200.20,
            'quantity' => 10
        ]);

        //print_r($result);

        $this->assertEquals(30, $result->getId());
        $this->assertEquals('Produto 1', $result->getNome());
        $this->assertEquals(200.20, $result->getPrice());
        $this->assertEquals(10, $result->getQuantity());
        $this->assertEquals(200.20 * 10, $result->getTotal());
    }

    public function testListProducts()
    {
        global $db;
        $product = new \SON\Model\Product($db);
        $result = $product->save([
            'name' => 'Produto 1',
            'price' => 200.2,
            'quantity' => 10
        ]);

        $products = $product->all();
        $this->assertCount(31, $products);

    }

}
