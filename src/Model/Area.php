<?php
/**
 * Created by PhpStorm.
 * User: tobias
 * Date: 03/05/18
 * Time: 22:20
 */

namespace SON;


class Area
{

    public function getArea($valor1, $valor2){
        return $valor1 * $valor2;
    }
}