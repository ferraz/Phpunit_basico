<?php
/**
 * Created by PhpStorm.
 * User: tobias
 * Date: 06/05/18
 * Time: 11:45
 */
declare(strict_types = 1);


namespace SON\Model;


class Product
{

    private $id;
    private $nome;
    private $price;
    private $quantity;
    private $total;

    private $pdo;

    /**
     * Product constructor.
     * @param $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }


    public function getId(): ?int
    {
        return (int)$this->id;
    }


    public function getNome(): ?string
    {
        return $this->nome;
    }


    public function setNome($nome): void
    {
        $this->nome = $nome;
    }


    public function getPrice(): ?float
    {
        return $this->price;
    }


    public function setPrice($price): void
    {
        $this->price = $price;
    }


    public function getQuantity(): ?int
    {
        return $this->quantity;
    }


    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }


    public function getTotal(): ?float
    {
        return $this->total;
    }

    private function hydrate(array $data)
    {
        $this->id = $data['id'];
        $this->setNome($data['name']);
        $this->setPrice($data['price']);
        $this->setQuantity($data['quantity']);
        $this->total = $data['total'];
    }


    public function save(array $data): Product
    {
        $query = "INSERT INTO products (`name`,`price`,`quantity`,`total`) VALUES (:name,:price,:quantity,:total)";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindValue(":name", $data['name']);
        $stmt->bindValue(":price", $data['price']);
        $stmt->bindValue(":quantity", $data['quantity']);
        $data['total'] = $data['price'] * $data['quantity'];
        $stmt->bindValue(":total", $data['total']);

        $stmt->execute();
        $data['id'] = $this->pdo->lastInsertId();
        $this->hydrate($data);
        return $this;
    }

    public function all()
    {
        $query = "SELECT * FROM products";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);

    }
}
